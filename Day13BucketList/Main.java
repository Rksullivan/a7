/**
 * A class that creates a bucket list and prints it.
 * 
 * @author unouser
 *
 */
public class Main {

	public static void main(String[] args) {
		
		///Instantiate Main so I don't have to use static everywhere.
		new Main();

	}
	
	
	
	final int maxListSize = 4;
	BucketListItem[] bucketList = new BucketListItem[maxListSize];
	
	public Main()
	{
		System.out.println("We're going to make a bucket list.");
		
		bucketList[0] = new BucketListItem("Golf in Scotland.");
		bucketList[1] = new BucketListItem("Go play soccer for Real Madrid.");
		bucketList[2] = new BucketListItem("Go spearfishing in Madagascar.");
		bucketList[3] = new BucketListItem("Do summerflips on the moon.");
		
		
		
		/*
		 * Starting Condition: I set my iterator to where I want to start. i = 0.
		 * Stopping Condition: When do I stop the loop? x < maxListSize
		 * Update my iterator: i++
		 * Body: What you do in the loop, printing out a bucket list item.
		 */
		for(int i = 0; i < maxListSize; i++)
		{
			System.out.println(bucketList[i]);
		}
		
		/* If we don't update the iterator--infinite loop
		for(int i = 0; i < maxListSize;)
		{
			System.out.println(bucketList[i]);
		}
		*/
		
		/* If we don't have a stopping condition, ArrayIndexOfOutBoundsException
		 * for(int i = 0; ; i++)
		{
			System.out.println(bucketList[i]);
		}
		 */
		
		///Is start by setting up my start condition (0)
		///I also pass in my stopping criteria
		customLoop(0, maxListSize, bucketList);
	}

	private void customLoop(int i, int stoppingCondition, BucketListItem[] inBucketList) {
		
		System.out.println("Starting loop with i=" + i);
		
		if(i < stoppingCondition)
		{
			//Do the body of our for loop
			System.out.println(inBucketList[i]);
			///Update the iterator and do it again.
			customLoop(i+1, stoppingCondition, inBucketList);
		}
		else
		{
			///Terminate my custom loop
			System.out.println("Terminating based on the stopping condition");
			return;
		}
		
		System.out.println("Ending with i=" + i);
		
	}
	
	

}
