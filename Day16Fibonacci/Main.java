/**
 * A simple class that calculate Fibonacci numbers
 * @author unouser
 *
 */
public class Main {

	/**
	 * The main method that starts the program 
	 * @param args Ignored
	 */
	public static void main(String[] args) {
		
		System.out.println("Welcome  to the Fibonacci number-get-you-hired-game.");
		
		int numberForWhichWeSeek = 100;
		
		int first = 0;
		int second = 1;
		
		int previousNumber = second;
		int previous2Number = first;
		
		int currentIndex = 2;
		
		/*while(numberForWhichWeSeek > currentIndex)
		{
			currentIndex++;
			int currentValue = previousNumber + previous2Number;
			previous2Number = previousNumber;
			previousNumber = currentValue;
			
			System.out.println("Index " + currentIndex + " has a value of " + currentValue);
			 
		}*/
		
		/*
		 * Recursion always needs:
		 * Stopping condition
		 * Startin state
		 * Body
		 * Change (increment)
		 */
		
		/*
		 * Starting condition
		 */
		int fib = getFibonacciNumber(numberForWhichWeSeek);
		
		System.out.println("The " + numberForWhichWeSeek +"th Fibonacci number is " + fib);
		

	}
	
	static int count = 0;

	/**
	 * Given a fibonacci number index, calculate its vlaue
	 * @param numberForWhichWeSeek The FibonacciNumber index were looking for
	 * @return The Fibonacci number itself
	 */
	private static int getFibonacciNumber(int index) {
		
		/*
		 * Stopping conditions
		 */
		
		//System.out.println(count++);
		
		if(index == 0)
			return 0;
		if(index== 1)
			return 1;
		
		return getFibonacciNumber(index - 1) + getFibonacciNumber(index -2);
					
	}

}
