/**
 * This class hold a prediction for a basketball team
 * @author unouser
 *
 */
public class Prediction {
	
	String teamName;
	
	int finalRound;

	/**
	 * Generate a prediction with the given team and final rank
	 * 
	 * @param string team name
	 * @param i final rank
	 */
	public Prediction(String string, int i) {
		teamName = string;
		finalRound = i;
	}

	/**
	 * @return the teamName
	 */
	public String getTeamName() {
		return teamName;
	}

	/**
	 * @param teamName the teamName to set
	 */
	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	/**
	 * @return the finalRound
	 */
	public int getFinalRound() {
		return finalRound;
	}

	/**
	 * @param finalRound the finalRound to set
	 */
	public void setFinalRound(int finalRound) {
		this.finalRound = finalRound;
	}
	
	@Override
	public String toString() {
		return teamName + " will end with a rank of " + finalRound;
	}

}
