/**
 * Not really. It's just super simple.
 * 
 * Let's add generics
 * 
 * 1. After the class declaration, add <T>
 * where T is the generic type we want Java to link at runtime
 * 
 * 2. Replace references to types with T
 * 
 * One catch...
 * You can't instantiate generic arrays in Java
 * The way you do it is  like this:
 * (T[])new Object[];
 * @author unouser
 *
 */
public class ArrayList<T> {

	T[] myItems = (T[])new Object[10];
	int nextEntry = 0;
	
	
	public void add(T inItem)
	{
		myItems[nextEntry] = inItem;
		nextEntry++;
	}
	
	public T get(int i)
	{
		return myItems[i];
	}
	
	public void print()
	{
		for(T s : myItems)
		{
			System.out.println(s);
		}
	
	}
	
}
