import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

public class MainPanel extends JPanel implements IView{

	
	JButton northButton = new JButton("North");
	JButton eastButton = new JButton("East");
	JButton southButton = new JButton("South");
	JButton westButton = new JButton("West");
	
	
	public MainPanel() {
		super();
		
		Main.game.addView(this);
		
		this.setLayout(new BorderLayout());
		
		this.add(northButton, BorderLayout.NORTH);
		this.add(eastButton, BorderLayout.EAST);
		this.add(southButton, BorderLayout.SOUTH);
		this.add(westButton, BorderLayout.WEST);
		this.add(new CenterPanel(), BorderLayout.CENTER);
		
		
		northButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.game.goNorth();
			}
			
		});
		
		eastButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.game.goEast();
			}
			
		});
		
		southButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.game.goSouth();
			}
			
		});
		
		westButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.game.goWest();
			}
			
		});
		
		
		this.setPreferredSize(new Dimension(600, 600));
		
		
		
		
	}
	
	@Override
	public void updated() {
		// TODO Auto-generated method stub
		
	}
	
	

	
	
}
