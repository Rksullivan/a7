import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class CenterPanel extends JPanel implements IView {

	
	JTextArea roomLabel = new JTextArea();
	
	
	
	
	
	public CenterPanel() {
		super();
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		roomLabel.setEnabled(false);
		roomLabel.setLineWrap(true);
		roomLabel.setWrapStyleWord(true);
		
		this.add(roomLabel);
		
		
		
		Main.game.addView(this);
	}
	
	@Override
	public void updated() {
		roomLabel.setText(Main.game.getRoomText());
		
		

	}

}
