
public abstract class GameObject {
	
	/** The name of the game object **/
	private String name;
	
	/** The description of the game object (if the player looks at it) **/
	private String description;
	
	public GameObject(String inName, String inDescription) {
		this.name = inName;
		this.description = inDescription;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	

}
