
public abstract class AAnimal implements IAnimal{

	private int happiness = 0;
	
	protected void improveHappiness()
	{
		happiness++;
	}
	
	@Override
	public String toString() {
		return "The " + this.getClass().getName() + " has a happiness value of " + happiness;
	}
	
	

}
