
public interface Stack<X> {

	boolean isEmpty();
	
	void add(X inType);
	
	X pop();
	
}
