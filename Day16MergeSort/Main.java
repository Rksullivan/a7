/**
 * A demonstration how to use he merge sort
 * @author unouser
 *
 */
public class Main {

	/**
	 * Our entry into the program
	 * @param args We ignore
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		int[] things = {20, 42, 15, 80, 7, 35, 200, -1, 1000000, 199, 5739};
		
		int[] newThings = new int[things.length];
		
		int middlePoint = things.length / 2;
		
		int[] leftHalf = sort(things, 0, middlePoint);
		int[] rightHalf = sort(things,  middlePoint + 1, things.length - 1);
		
		int iL = 0;
		int iR = 0;
		int iN = 0;
		
		while(iN < things.length)
		{
			if(leftHalf[iL] < rightHalf[iR])
			{
				newThings[iN] = leftHalf[iL];
				iL++;
				iN++;
			}
			else
			{
				newThings[iN] = rightHalf[iR];
				iR++;
				iN++;
			}
		}
	}

	private static int[] sort(int[] things, int start, int end) {
		
		if(start == end)
		{
			return new int[]{things[start]};
		}
		else if(end == start + 1)
		{
			if(things[start] <things[end])
				return new int[]{things[start], things[end]};
			else
				return new int[]{things[end], things[start]};
		}

		int[] newThings = new int[end - start];
		
		int middlePoint = (end - start) / 2;
		
		int[] leftHalf = sort(things, start, middlePoint);
		int[] rightHalf = sort(things,  middlePoint + 1, end);
		
		int iL = 0;
		int iR = 0;
		int iN = 0;
		
		while(iN < end - start)
		{
			if(leftHalf[iL] < rightHalf[iR])
			{
				newThings[iN] = leftHalf[iL];
				iL++;
				iN++;
			}
			else
			{
				newThings[iN] = rightHalf[iR];
				iR++;
				iN++;
			}
		}
		
		return newThings;
	}
	
	

}
