
public interface List {

	void add(String string);

	int size();

	String get(int i);

	boolean removeAt(int i);

}
