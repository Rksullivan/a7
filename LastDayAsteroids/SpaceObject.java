
public class SpaceObject {

	/** The radius of all asteroids. */
	public static final float ASTEROID_RADIUS = 1;
	
	public float radius;
	
	/** The location of the asteroid. */
	protected Point location;

	public SpaceObject() {
		super();
	}

	/**
	 * Gets the location.
	 *
	 * @return the location
	 */
	public Point getLocation() {
		return location;
	}

	/**
	 * Sets the location.
	 *
	 * @param location the new location
	 */
	public void setLocation(Point location) {
		this.location = location;
	}

	/**
	 * Move.
	 *
	 * @param x the amount by which the asteroid will move in x
	 * @param y the amount by which the astetroid will move in y
	 */
	public void move(float x, float y) {
		location.update(location.x + x, location.y + y);
		
	}

}