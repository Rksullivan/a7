
public class FightABear extends Activity{
	
	public FightABear() {
		name = "fight bear";
		
		bonus.providesFood += 100;
		bonus.providesWater += 100;
		
		probabilityOfSuccess = .1;
		HoursRequired = 12;
	}

}
