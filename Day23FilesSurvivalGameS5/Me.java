import java.util.ArrayList;
import java.util.List;

public class Me {
	
	public static final int MAX_WATER = 100;
	public static final int MAX_FOOD = 100;
	public static final int MAX_REST = 100;
	public static final int MAX_HEAT = 100;
	public static final int WIN_HOURS = 240;
	
	public int water;
	public int food;
	public int rest;
	public int heat;
	public int hours;
	
	DayHourlyBonus dayBonus = new DayHourlyBonus();
	NightHourlyBonus nightHourlyBonus = new NightHourlyBonus();
	
	
	public Me() {
		
		water = MAX_WATER;
		food = MAX_FOOD;
		rest = MAX_FOOD;
		heat = MAX_FOOD;
		hours = 0;
		
		
	}

	public String getTime() {
		
		int hoursPastMidnight = hours % 24;
		boolean isDaylight = hoursPastMidnight < 12;
		int remaining = 12 - (hoursPastMidnight % 12);
		
		
		String toReturn = "You have survived for " + hours + " hours.";
		
		if(isDaylight)
			toReturn += " You have " + remaining + " hours of daylight remaining.";
		else
			toReturn += " You have " + remaining + " hours of night remaining.";
		
		return toReturn;
	}

	public boolean isDead() {
		return water < 0 || food < 0 || rest < 0 || heat < 0; 
	}

	public void elapseTime(int hoursRequired) {
		
		
		for(int inc = 0; inc < hoursRequired; inc++)
		{
			Bonus currentBonus = nightHourlyBonus;
			if(isDaylight())
				currentBonus = dayBonus;
			
			food += currentBonus.providesFood;
			water += currentBonus.providesWater;
			rest += currentBonus.providesRest;
			heat += currentBonus.providesHeat;
			
			hours++;
			
		}
		
		
		
		
	}

	private boolean isDaylight() {
		int hoursPastMidnight = hours % 24;
		return hoursPastMidnight < 12;
	}

	public String getStats() {
		return "Food: " + food + "\r\n" + "Water: " + water + "\r\nRest: " + rest + "\r\nHeat: " + heat;
	}

	public boolean isRescued() {
		return hours >= WIN_HOURS;
	}

	public void addBonus(Bonus bonus) {
		food += bonus.providesFood;
		water += bonus.providesWater;
		heat += bonus.providesHeat;
		rest += bonus.providesRest;		
		
		food = Math.min(MAX_FOOD, food);
		water = Math.min(MAX_WATER, water);
		heat = Math.min(MAX_HEAT, heat);
		rest = Math.min(MAX_REST, rest);
	}

}
