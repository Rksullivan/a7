import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		new Main();
	}
	
	
	
	Me me = new Me();
	
	Scanner scanner = new Scanner(System.in);
	
	List<Activity> activities = new ArrayList<Activity>();
	
	public Main() {
	
		
		activities.add(new RestActivity());
		activities.add(new ForageActivity());
		activities.add(new HuntActivity());
		activities.add(new DistillWater());
		activities.add(new BuildFireActivity());
		activities.add(new FishingActivity());
		activities.add(new FightABear());
		
		System.out.println("While flying on a FedEx airplane, the engine suddenly burst into the flame,\r\nthe pilot has a heart attack, and the copilot passes out from fright.\r\nConstructing a crude parachute out of a flashlight and fire extinguisher,\r\nyou float down to the ocean. After many hours, the current takes you to an island\r\nthat is deserted except for a random volley ball.\r\nCan you survive???!?!??!\r\n");
		
		while(true)
		{
			System.out.println("\r\n");
			if(me.isDead())
			{
				System.out.println("You died. That's too bad. Bring more supplies next time.");
				break;
			}
			if(me.isRescued())
			{
				System.out.println("Horray! You were rescued....But living out here was so relaxing. No work, no worries, just great times and a great tan. You scare off the search party and go back to living the dream. Berries anyone? ");
				break;
			}
			
			System.out.println(me.getTime());
			System.out.println(me.getStats());
			
			System.out.print(">");
			
			String toDo = scanner.nextLine();
			
			boolean foundActivity = false;
			
			if(toDo.toLowerCase().startsWith("save"))
			{
				///Get the file name 
				
				///grab a subsring starting at the first spcae
				
				String filename = toDo.substring(toDo.indexOf(' ') + 1);
				
				
				
				/* Steps to saving
				 * Choose binary or character (we'll always use character.)
				 * Create a list of strings to save
				 * save those to a file 
				 */
				
				List<String> toSave = new ArrayList<String>();
				toSave.add("" + me.food);
				toSave.add("" + me.water);
				toSave.add("" + me.rest);
				toSave.add("" + me.heat);
				toSave.add("" + me.hours);
				
				
				try {
					Files.write(Paths.get(filename), toSave, StandardCharsets.UTF_8);
				} catch (IOException e) {
					System.out.println("Hey, that didn't work. Pick a REAL filename.");
				}
				
				foundActivity = true;
			}
			else if(toDo.toLowerCase().startsWith("load"))
			{
				try {
					
					String filename = toDo.substring(toDo.indexOf(' ') + 1);
					
					
					List<String> myLines = Files.readAllLines(Paths.get(filename), StandardCharsets.UTF_8);
					
					me.food = Integer.parseInt(myLines.get(0));
					me.water = Integer.parseInt(myLines.get(1));
					me.rest = Integer.parseInt(myLines.get(2));
					me.heat = Integer.parseInt(myLines.get(3));
					me.hours = Integer.parseInt(myLines.get(4));
					
					foundActivity = true;
					
				} catch (IOException e) {
					System.out.println("Sorry, that file seems bad. Try again.");
				}
				
				
				
			}
			
			for(Activity activity : activities)
			{
				if(activity.name.toLowerCase().equals(toDo.toLowerCase()))
				{
					foundActivity = true;
					me.elapseTime(activity.HoursRequired);
					
					if(activity.isSuccessful())
					{
						System.out.println("Success, it worked!");
						me.addBonus(activity.bonus);
						
					}
					else
					{
						System.out.println("Well, that didn't work out to well. You didn't gain anything from that. :(");
					}
					
					continue;
				}
			}
			
			if(!foundActivity)
				System.out.println("Sorry, I didn't get that, will you try again?");
			
			
			
		}
		
	}

}
