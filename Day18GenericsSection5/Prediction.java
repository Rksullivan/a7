
public class Prediction implements HasName {
	
	String teamName;
	
	int finalRank;
	
	public Prediction(String string, int i) {
		
		teamName = string;
		finalRank = i;
	}

	public String getTeamName() {
		return teamName;
	}

	public void setTeamName(String teamName) {
		this.teamName = teamName;
	}

	public int getFinalRank() {
		return finalRank;
	}

	public void setFinalRank(int finalRank) {
		this.finalRank = finalRank;
	}
	
	@Override
	public String toString() {
		return teamName + " will have a final rank of " + finalRank;
	}

	@Override
	public String getName() {
		return teamName;
	}
}
