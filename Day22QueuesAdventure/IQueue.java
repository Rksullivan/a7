/**
 * A queue interface.
 *
 * @param <X>
 */
public interface IQueue<X> {

	boolean isEmpty();
	
	void add(X inType);
	
	X pop();
	
}
