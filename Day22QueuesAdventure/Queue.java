
public class Queue<X> implements IQueue<X> {

	Node<X> first = null;
	Node<X> last = null;

	@Override
	public boolean isEmpty() {
		return first == null;
	}

	@Override
	public void add(X inType) {
		
		Node<X> newNode = new Node<X>();
		newNode.data = inType;
		
		if(first == null)
		{
			first = newNode;
			last = newNode;
		}
		else
		{
			last.previousNode = newNode;
			last = newNode;
		}
		

	}

	@Override
	public X pop() {
		if(isEmpty())
		{
			return null;
		}
		else{
			X toReturn = first.data;
			first = first.previousNode;
			return toReturn;
		}
	}

	
}
