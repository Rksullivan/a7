
public class Balanced extends Hero {

	public Balanced() {
		super(90, 15, 15, 2, "Throws a medium snow ball.", "50% chance that the other player's turn is skipped.");
		
	}

	@Override
	public void special(Player me, Player opponent) {
		for(Hero hero : opponent.heroes)
		{
		if(Math.random() < .3)
			//if(true)
		{
			hero.buffs.add(new SkipTurnBuff());
		}
		}

	}

}
