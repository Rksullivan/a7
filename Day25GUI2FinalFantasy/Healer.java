
public class Healer extends Hero {

	public Healer() {
		super(80, 10, 4, 3, "Throws a very light snowball", "Give hot cocoa to all allies to warm them up.");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void special(Player me, Player opponent) {
		for(Hero hero : me.heroes)
		{
			if(hero != this)
			{
				hero.gainHealth(20);
			}
		}

	}

}
