import java.util.ArrayList;
import java.util.List;

public abstract class Hero {
	
	 int health;
	
	 int attack;
	
	 int defense;
	 
	 int healthMax;
	 
	 String attackString;
	 
	 String specialString;
	 
	 List<Buff> buffs = new ArrayList<Buff>();
	 
	 boolean canAttack = false;
	 
	 int specialCooldown = 0;
	 
	 int coolDownTime;
	 
	 public Hero(int inHealthMax, int inAttack, int inDefense, int inSpecialCooldown, String inAttackString, String inSpecialString) {
		
		 health = inHealthMax;
		 healthMax = inHealthMax;
		 attack = inAttack;
		 defense = inDefense;
		 attackString = inAttackString;
		 specialString = inSpecialString;
		 coolDownTime = inSpecialCooldown;
	}
	 
	 public abstract void special(Player me, Player opponent);

	public void takeDamage(int damage) {
		
		for(Buff buff : buffs)
		{
			if(buff instanceof InvincibleBuff)
			{
				return;
			}
		}
		
		health -= damage / Math.log(defense);
		
		checkHealth();
		
	}
	
	private void checkHealth()
	{
		health = Math.max(0, health);
		health = Math.min(healthMax, health);
	}
	
	public boolean isAlive(){
		return health > 0;
	}

	public void gainHealth(int i) {
		health += i;
		checkHealth();
		
	}

	public void startTurn() {
		canAttack = true;
		specialCooldown -= 1;
		specialCooldown = Math.max(0, specialCooldown);
		
	}
	
	public boolean canUseCooldown()
	{
		if(specialCooldown > 0)
			return false;
		
		for(Buff buff : buffs)
		{
			if(buff instanceof SkipTurnBuff)
				return false;
		}
		
		return true;
	}

	public boolean getCanAttack() {
		if(!canAttack)
			return false;
		for(Buff buff : buffs)
		{
			if(buff instanceof SkipTurnBuff)
				return false;
		}
		
		return true;
	}
	

}
