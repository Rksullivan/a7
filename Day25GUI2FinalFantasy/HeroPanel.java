import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

@SuppressWarnings("serial")
public class HeroPanel extends JPanel implements View {
	
	Hero myHero;
	
	JLabel health = new JLabel();
	
	JLabel buffs = new JLabel();
	
	JLabel cooldown = new JLabel();
	
	JButton selectButton = new JButton("Select");
	
	public HeroPanel(Hero inHero) {
		super();
		
		Main.model.addView(this);
		
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		
		
		myHero = inHero;
		
		this.add(new JLabel("-------------------"));
		
		this.add(new JLabel(myHero.getClass().getName()));
		this.add(health);
		this.add(cooldown);
		this.add(buffs);
		this.add(selectButton);
		
		selectButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.model.selectHero(myHero);
				
			}
		});
		
		this.add(new JLabel("~~~~~~~~~~~~~~~~~~~"));
		
		
	}

	@Override
	public void update() {
		
		if(!myHero.isAlive())
		{
			health.setText("Ran back inside to warm up by the fire.");
			cooldown.setText("-");
			buffs.setText("-");
			selectButton.setEnabled(false);
			return;
			
		}
		
		if(Main.model.isThisHerosTurn(myHero))
		{
			selectButton.setEnabled(true);
		}
		else
		{
			selectButton.setEnabled(false);
		}
		
		if(Main.model.selectedHero == myHero)
		{
			this.setBackground(Color.YELLOW);
		}
		else
			this.setBackground(Color.WHITE);
		
		health.setText("Health: " + myHero.health);
		cooldown.setText("Special Cooldown: " + myHero.specialCooldown);
		buffs.setText("Buffs: ");
		if(myHero.buffs.size() == 0)
			buffs.setText("No buffs.");
		for(Buff buff : myHero.buffs)
		{
			buffs.setText(buffs.getText() + "\r\n" + buff.getClass().getName() + " +" + buff.amount + " "+ buff.turns + " turns remaining");
		}
	}

}
