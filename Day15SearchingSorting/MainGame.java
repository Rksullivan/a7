import java.util.Random;
import java.util.Scanner;

/**
 * The main class for my really unique guess the number game 
 * @author unouser
 *
 */
public class MainGame {
	
	/** The secret number the user is trying to guess */
	static int mySecretNumber;
	
	/** The first message the user will see. */
	static String welcomeString = "Hi. Welcome to my pancake game.";
	
	public static void main(String[] args) {
		
		System.out.println(welcomeString);
		
		mySecretNumber = new Random().nextInt(1000000);
		
		GuessingGenerator generator = new Smart();
		
		while(true)
		{
			System.out.println("Can you guess my number?");
			
			int guess = generator.nextGuess();
			generator.addGuess(guess);
			System.out.println("You guessed " + guess);
			
			if(guess == mySecretNumber)
				break;
			else
			{
				if(guess > mySecretNumber)
				{
					System.out.println("Nice try. Try lower next time.");
					generator.guessToHigh();
				}
				else
				{
					System.out.println("Bad try. Try higher next time.");
					generator.guessToLow();
				}
				
			}
			
		}
		
		System.out.println("Holy Pancakes, you guessed the right number.");
	}

}
