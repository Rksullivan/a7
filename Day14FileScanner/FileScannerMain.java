import java.io.File;


public class FileScannerMain {

	public static void main(String[] args) {
		System.out.println("Welcome to the file scanner.");
		
		File cDrive = new File("c:/");
		scan(cDrive);
		
		/*File[] children = cDrive.listFiles();
		
		for(int i = 0; i < children.length; i++)
		{
			File child = children[i];
			System.out.println(child.getAbsolutePath());
			
			if(child.isDirectory())
			{
				File[] children_02 = child.listFiles();
				if(children_02 != null)
				{
					for(int j = 0; j < children_02.length; j++)
					{
						File child_02 = children_02[j];
						System.out.println("   " + child_02.getAbsolutePath());
						
						if(child_02.isDirectory())
						{
							File[] children_03 = child_02.listFiles();
							if(children_03 != null)
							{
								for(int k = 0; k < children_03.length; k++)
								{
									File child_03 = children_03[k];
									System.out.println("      " + child_03.getAbsolutePath());
								}
							}
						}
							
					}
				}
			}
		}*/

	}

	/*
	 * All recursive custom loops need:
	 * 1. Start state (C:/)
	 * 2. A stopping condition - no more directories
	 * 3. Body - print out the names of files
	 * 4. Post-body change - move on to child directories
	 * 
	 */
	private static void scan(File start) {
		
		/* Stopping condition */
		File[] children = start.listFiles();
		if(children == null || children.length == 0)
		{
			/* We need to stop. */
			return;
		}
		
		/* Body */
		for(int i = 0; i < children.length; i++)
		{
			File child = children[i];
			System.out.println(child.getAbsolutePath());
			
			/* post-body change */
			if(child.isDirectory())
				scan(child);
		}
		
		
	}

}
